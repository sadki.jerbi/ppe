FROM node:16
COPY . /app
WORKDIR /app
RUN npm install || true
EXPOSE 8000
CMD ["npm", "start"]
