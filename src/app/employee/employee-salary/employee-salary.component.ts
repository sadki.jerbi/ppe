import { Component, Input, OnInit, Output } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { EmployeeForCreating } from 'src/app/_interfaces/employee-create';
import { ErrorHandlerService } from 'src/app/shared/services/error-handler.service';

@Component({
  selector: 'app-employee-salary',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeSalaryComponent implements OnInit {
  public errorMessage = '';

  public employeeForm: FormGroup;

  valeurcnss: number;
  valeurbrut: number;
  valeurimpo: number;
  valeurRetenu : number ;



  constructor(private repo: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router) { }

  ngOnInit() {
    this.employeeForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      startDate: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      position: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      phoneNumber: new FormControl('', [Validators.required]),
      salary: new FormControl('', [Validators.required]),
      hourlyRate: new FormControl('', [Validators.required]),
      Retenu: new FormControl('', [Validators.required])
    });
  }

  public validateControl(controlName: string) {
    if (this.employeeForm.controls[controlName].invalid && this.employeeForm.controls[controlName].touched) {
      return true;
    }
    return false;
  }

  public hasError(controlName: string, errorName: string) {
    if (this.employeeForm.controls[controlName].hasError(errorName)) {
      return true;
    }
    return false;
  }

  public executeDatePicker(event: any) {
    this.employeeForm.patchValue({ startDate: event });
  }

  public createEmployee(employeeFormValue: any) {

     this.valeurcnss=employeeFormValue.email * 0.09 ;
     this.valeurbrut= employeeFormValue.email ;
     this.valeurimpo = employeeFormValue.email - employeeFormValue.Retenu;


     if(employeeFormValue.name == 1) {
      this.valeurRetenu =  90 ;

     }else if (employeeFormValue.name==2) {
      this.valeurRetenu =  75 ;

     } else {this.valeurRetenu =  60 ;

      
     }

     
    
  }

  private executeEmployeeCreation(employeeFormValue: { name: any; startDate: Date;
    email: any; position: any; phoneNumber: any; salary: any; hourlyRate: any; }) {
    const employee: EmployeeForCreating = {
      name: employeeFormValue.name,
      position: employeeFormValue.position,
      startDate: employeeFormValue.startDate,
      email: employeeFormValue.email,
      phoneNumber: employeeFormValue.phoneNumber,
      salary: employeeFormValue.salary,
      hourlyRate: employeeFormValue.hourlyRate
    };

    const apiUrl = 'employees/';
    this.repo.create(apiUrl, employee)
      .subscribe(res => {
        // $('#successModal').modal('show');
        // document.getElementById('successModal').click();
        this.router.navigate(['employee/salary']);
      },
      (error => {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
    );
  }

  public redirectToEmployeeList() {
    this.router.navigate(['/employee/salary']);
  }
}
